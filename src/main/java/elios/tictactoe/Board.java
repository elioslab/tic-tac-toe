package elios.tictactoe;

import java.io.Serializable;

public class Board implements Serializable
{
    private Players[] board;
    private int size;
    private GameStatus winner;

    public Board(int size) 
    {
        this.size = size;
        this.winner = GameStatus.ERROR;
        
        this.board = new Players[size * size];
        
        for(int i=0; i<board.length; i++)
            board[i] = Players.EMPTY;
    }

    public GameStatus getWinner() {
        return winner;
    }

    public void setWinner(GameStatus winner) {
        this.winner = winner;
    }
    
    public Players[] getBoard() {
        return board;
    }

    public int[] getBoardValues()
    {
        int[] boardvalues = new int[board.length];
        for(int i=0; i<board.length; i++)
        {
            if(board[i] == Players.MAX)
                boardvalues[i] = 1;
            else if (board[i] == Players.MIN)
                boardvalues[i] = -1;
            else
                boardvalues[i] = 0;
        }
        return boardvalues;
    }
    
    public Board duplicate()
    {
        Board newBoard = new Board(size);
        for (int i=0; i<this.board.length; i++)
            newBoard.setValue(i, getValue(i));
        
        newBoard.winner = this.winner;
        
        return newBoard;
    }
    
    public int getSize() {
        return size;
    }
    
    public boolean isFull()
    {
        for (Players cell : board) {
            if (cell == Players.EMPTY) {
                return false;
            }
        }
        return true;
    }
    
    public boolean isFinalAround(int position)
    {
        if(position == -1)
            return false;
        
        Players player = board[position];
	Point p = this.index2matrix(position);
	int pointRow = p.getRow();
	int pointCol = p.getCol();

	Players[][] r = new Players[5][5]; 
	
	for(int i=0; i<5; i++)
        {
            int row = pointRow+i-2;
            
            for(int j=0; j<5; j++)
            {
                int col = pointCol+j-2;
            
                if( (row<0) || (row>=size) || (col<0) || (col>=size))
                    r[i][j] = Players.EMPTY;
                else
                    r[i][j] = this.getValue(row , col);
            }
        }

	if( ((player==r[2][3]) && (player==r[2][4])) ||
	    ((player==r[2][3]) && (player==r[2][1])) ||
            ((player==r[2][0]) && (player==r[2][1])) ||
	
            ((player==r[1][2]) && (player==r[0][2])) ||
            ((player==r[1][2]) && (player==r[3][2])) ||
	    ((player==r[3][2]) && (player==r[4][2])) ||

            ((player==r[1][3]) && (player==r[0][4])) ||
            ((player==r[1][3]) && (player==r[3][1])) ||
            ((player==r[3][1]) && (player==r[4][0])) ||

            ((player==r[1][1]) && (player==r[0][0])) ||
            ((player==r[1][1]) && (player==r[3][3])) ||
            ((player==r[3][3]) && (player==r[4][4])) )
		return true;
	else
            return false;
    }
    
    public void setValue(int index, Players value)
    {
	if( (index<board.length) && (index>=0) )
            board[index] = value;
    	else
            throw(new IndexOutOfBoundsException("Index=" + index + " is out of bounds!"));
    }
    
    public void setValue(int row, int col, Players value)
    {
        int index = matrix2index(row, col);
        
	if( (index<board.length) && (index>=0) )
            board[index] = value;
    	else
            throw(new IndexOutOfBoundsException("(Row, Col)=(" + row + "," + col  + ") is out of bounds!"));
    }

    public Players getValue(int index)
    {
	if( (index<board.length) && (index>=0) )
            return board[index];
    	else
            throw(new IndexOutOfBoundsException("Index=" + index + " is out of bounds!"));
    }
    
    public Players getValue(int row, int col)
    {
        int index = matrix2index(row, col);
        
	if( (index<board.length) && (index>=0) )
            return board[index];
    	else
            throw(new IndexOutOfBoundsException("(Row, Col)=(" + row + "," + col  + ") is out of bounds!"));
    }

    public Point index2matrix(int index)
    {
	int row = index / size;
	int col = index % size; 
        
        Point p = new Point(row, col);
	
	return p;
    }

    public int matrix2index(int row, int col)
    {
	int position = row * size + col;

        return position;
    }
    
    public void print()
    {
        System.out.println();
	for(int i=0; i<size; i++)
        {
            for(int j=0; j<size; j++)
            {
                String sign = "";
                
                if(getValue(i,j) == Players.MAX)
                    sign = "X";
                else if(getValue(i,j) == Players.MIN)
                    sign = "O";
                else if(getValue(i,j) == Players.EMPTY)
                    sign = "_";
                System.out.print(sign + " ");
            }
            System.out.println();
	}
    }
}
