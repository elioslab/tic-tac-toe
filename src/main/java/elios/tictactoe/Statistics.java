package elios.tictactoe;

import java.util.ArrayList;
import java.util.List;

public class Statistics 
{
    private List<GameStatus> values;

    public Statistics() {
        values = new ArrayList<>();
    }
    
    public void add(GameStatus value)
    {   
        values.add(value);
    }
    
    public int number(GameStatus value)
    {
        int sum = 0;
        for(int i=0; i<values.size(); i++)
            if(values.get(i) == value)
                sum++;
        return sum;
    }
    
    public int number(GameStatus value, int last)
    {
        if(last >= values.size())
            return number(value);
        else
        {
            int sum = 0;
            for(int i=values.size()-last; i<values.size(); i++)
                if(values.get(i) == value)
                    sum++;
            return sum;
        }
    }
    
    public float freq(GameStatus value)
    {
        return (float)number(value)/values.size();
    }
        
    public float freq(GameStatus value, int last)
    {
        if(last >= values.size())
            return (float)number(value)/values.size();
        else
            return (float)number(value, last)/last;
    }
    
    public void reset()
    {
        values = new ArrayList<>();
    }
            
    public int size()
    {
        return values.size();
    }
}
