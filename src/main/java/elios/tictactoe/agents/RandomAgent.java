package elios.tictactoe.agents;

import elios.tictactoe.Board;
import elios.tictactoe.Players;
import elios.tictactoe.GameStatus;
import elios.tictactoe.agents.Agent;

public class RandomAgent implements Agent
{
    //private Random random;
    
    @Override
    public void init() 
    {

    }

    @Override
    public int nextMove(Board board) 
    {
        int number = (int) Math.ceil(Math.random() * board.getSize() * board.getSize());
        int index = -1;
        
        while (number > 0)
        {
            index++;
            if(index >= board.getSize()*board.getSize())
               index = 0;
            
            if(board.getValue(index) == Players.EMPTY)
                number--;
        }

        return index;
    }

    @Override
    public void update(GameStatus result, Board board) {
    }

    @Override
    public void save() {

    }

    @Override
    public void load() {

    }
}
