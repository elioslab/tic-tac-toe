package elios.tictactoe.agents;

import elios.tictactoe.Board;
import elios.tictactoe.GameStatus;

public interface Agent 
{
    public void init();
    public void update(GameStatus result, Board board);
    public int nextMove(Board board);
    public void save();
    public void load();
}
