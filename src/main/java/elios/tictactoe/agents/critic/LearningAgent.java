package elios.tictactoe.agents.critic;

import elios.tictactoe.Board;
import elios.tictactoe.Players;
import elios.tictactoe.GameStatus;
import elios.tictactoe.agents.Agent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LearningAgent implements Agent
{
    private final int size;
    private final float etaReward;
    private final float etaPunish;
    private final float etaIncorrect;
    private float w0;
    
    private final Boolean load;
    
    private final Players player;

    private final TimeMachine timemachine;
    private final NeuronMemory memory;

    List<Neuron> neurons;

    public LearningAgent(Players player, int boardsize, boolean load)
    {
        this.player = player;
        this.size = boardsize * boardsize;
        this.neurons = new ArrayList<>();
        this.etaPunish = -0.001f;
        this.etaReward = 0.001f;
        this.etaIncorrect = -0.0001f;
        this.load = load;
               
        for(int i=0; i<size; i++)
            neurons.add(new Neuron(size, i));
        
        timemachine = new TimeMachine();
        memory = new NeuronMemory();
        
        memory.add(neurons);
    }
    
    @Override
    public void init() 
    {
        timemachine.reset();
    }
    
    public void reset()
    {
        for (int i=0; i<neurons.size(); i++)
            neurons.get(i).reset();
    }
    
    private int nthBestMove(Board board, int nth)
    {
        List<Neuron> orderedlist = new ArrayList<>();
        for(int i=0; i<neurons.size(); i++)
            orderedlist.add(neurons.get(i));
        
        Collections.sort(orderedlist, new Comparator<Neuron>(){
            public int compare(Neuron n1, Neuron n2)
            {
                if (n1.spike(board.getBoardValues()) > n2.spike(board.getBoardValues()))
                    return -1;
                else if (n1.spike(board.getBoardValues()) < n2.spike(board.getBoardValues()))
                    return 1;
                else
                    return 0;
            }
        });

        return orderedlist.get(nth).getPosition();
    }
    
    @Override
    public int nextMove(Board board) 
    {
        List <Integer> incorrect = new ArrayList<>();
        
        for(int i=0; i<neurons.size(); i++)
        {
            int move = nthBestMove(board, i);
            
            if(board.getValue(move) != Players.EMPTY)
            {
                incorrect.add(move);
            }
            else
            {
                for(int j=0; j<incorrect.size(); j++)
                    reward(board, incorrect.get(j), etaIncorrect);
                
                timemachine.addBoard(board, move);
                
                return move;
            }
        }
        throw(new IndexOutOfBoundsException("No valid move found!"));
    }
    
    private void reward(Board board, int decision, float weight)
    {
        Neuron neuron = neurons.get(decision);
        neuron.update(weight, board.getBoardValues());
    }
    
    public void update(GameStatus result, Board board)
    {
        for(int i=0; i<timemachine.size(); i++)
        {
            Board temp_board = timemachine.getBoard(i);
            Integer decision = timemachine.getDecision(i);

            if(player == Players.MAX)
                if(result == GameStatus.MAX)
                    reward(temp_board, decision, etaReward);

            if(player == Players.MIN)
                if(result == GameStatus.MIN)
                    reward(temp_board, decision, etaReward);

            if(player == Players.MAX)
                if(result == GameStatus.MIN)
                    reward(temp_board, decision, etaPunish);

            if(player == Players.MIN)
                if(result == GameStatus.MAX)
                    reward(temp_board, decision, etaPunish);
        }
        
        memory.add(neurons);
        //memory.printDifference();
        
        //reset();
    }
    
    public final void load()
    {
        if(load)
        {
            InputStream file = null;
            try 
            {
               if (Files.exists(Paths.get("neurons.ser"))) 
               {
                    file = new FileInputStream("neurons.ser");
                    InputStream buffer = new BufferedInputStream(file);
                    ObjectInput input = new ObjectInputStream (buffer);

                    neurons = (List<Neuron>)input.readObject();
               }
            } 
            catch (FileNotFoundException ex) 
            {
                Logger.getLogger(LearningAgent.class.getName()).log(Level.SEVERE, null, ex);
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(LearningAgent.class.getName()).log(Level.SEVERE, null, ex);
            } 
            catch (ClassNotFoundException ex) 
            {
                Logger.getLogger(LearningAgent.class.getName()).log(Level.SEVERE, null, ex);
            } 
            finally 
            {
                try 
                {
                    if (Files.exists(Paths.get("neurons.ser"))) 
                    {
                        file.close();
                    }
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(LearningAgent.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void save()
    {
        try
        {
            OutputStream file = new FileOutputStream("neurons.ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            output.writeObject(neurons);
            output.close();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(LearningAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
