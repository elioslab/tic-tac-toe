package elios.tictactoe.agents.critic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Neuron implements Serializable
{
    private final List<Float> w;
    private final float w0;
    private float value;
    private final int size;
    private final int position;
    
    public int size()
    {
        return size;
    }
    
    public Neuron duplicate()
    {
        Neuron newNeuron = new Neuron(this.size, this.position);
        for(int i=0; i<size+1; i++)
            newNeuron.w(i, this.w(i));
        return newNeuron;
    }

    public int getPosition() {
        return position;
    }
    
    public float w(int index)
    {
        return w.get(index);
    }
    public void w(int index, float value)
    {
        w.set(index, value);
    }
    
    public void reset()
    {
        for(int i=0; i<size+1; i++)
            w.set(i, w0 * (float)Math.random());
    }
    
    public Neuron(int size, int position)
    {
        this.w = new ArrayList<>();
        this.value = 0;
        this.size = size;
        this.position = position;
        
	w0 = 0.01f;

	for(int i=0; i<size+1; i++)
            w.add(w0 * (float)Math.random());
    }

    public float spike(int input[])
    {
	value = w.get(0);

	for(int i=0; i<w.size()-1; i++)
            value += w.get(i+1) * input[i];

	return value;
    }

    public void update(float dw, int input[])
    {
	w.set(0, w.get(0) + dw);

	for(int i=0; i<w.size()-1; i++)
            w.set(i+1, w.get(i+1) + dw * input[i]);
    }
    
    public void print()
    {
        for(int i=0; i<w.size(); i++)
            System.out.print(w.get(i) + " ");
        System.out.println();
    }
}