package elios.tictactoe.agents.critic;

import java.util.ArrayList;
import java.util.List;

public class NeuronMemory 
{
    private List<List<Neuron>> memory;

    public NeuronMemory() {
        memory = new ArrayList<>();
    }
    
    public void add(List<Neuron> neurons)
    {
        List<Neuron> newNeurons = new ArrayList<>();
        
        for(int i=0; i<neurons.size(); i++){
            newNeurons.add(neurons.get(i).duplicate());
        }
        memory.add(newNeurons);
    }

    public void printDifference()
    {
        List<Neuron> init_neurons = memory.get(0);
        List<Neuron> last_neurons = memory.get(memory.size()-1);
        
        System.out.println();
        for(int i=0; i< init_neurons.size(); i++)
        {
            Neuron init_neuron = init_neurons.get(i);
            Neuron last_neuron = last_neurons.get(i);
            
            for(int j=0; j<init_neuron.size(); j++)
            {
                System.out.print(init_neuron.w(j) - last_neuron.w(j) + " ");
            }
            System.out.println();
        }
    }
    
    
}
