package elios.tictactoe.agents.critic;

import elios.tictactoe.Board;
import java.util.ArrayList;
import java.util.List;

public class TimeMachine 
{
    private List<Board> boards;
    private List<Integer> decisions;

    public TimeMachine() 
    {
        boards = new ArrayList<>();     
        decisions = new ArrayList<>();
    }

    public void reset()
    {
        boards = new ArrayList<>();     
        decisions = new ArrayList<>();
    }
           
    public int size()
    {
        return boards.size();
    }
    
    public void addBoard(Board board, Integer decision)
    {
        Board newBoard = board.duplicate();
        boards.add(newBoard);
        decisions.add(decision);
    }
        
    public Board getBoard(int time)
    {
	if( (time<boards.size()) && (time>=0) )
            return boards.get(time);
	else
            throw(new IndexOutOfBoundsException("Time=" + time + " is out of bounds!"));
    }
    
    public Integer getDecision(int time)
    {
	if( (time<decisions.size()) && (time>=0) )
            return decisions.get(time);
	else
            throw(new IndexOutOfBoundsException("Time=" + time + " is out of bounds!"));
    }
}
