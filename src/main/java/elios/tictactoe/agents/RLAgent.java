package elios.tictactoe.agents;

import elios.tictactoe.Board;
import elios.tictactoe.Players;
import elios.tictactoe.GameStatus;
import wondertech.learner.Learner;
import wondertech.learner.Status;

public class RLAgent implements Agent {
    private Learner learner;
    private Players player;
    
    public RLAgent(Players player) {
        this.player = player;
        learner = new Learner(9, 9);
    }

    @Override
    public void init() {
    }

    @Override
    public void update(GameStatus result, Board board) {
        Double reward = 0.0;
        
        if(result == GameStatus.MAX){
            if(player == Players.MAX)
                reward = 1.0;
            else
                reward = -1.0;;
        }
        else if(result == GameStatus.MIN){
            if(player == Players.MIN)
                reward = 1.0;
            else
                reward = -1.0;
        }
        else if(result == GameStatus.ERROR){
            reward = -10.0;
        }
        else if(result == GameStatus.DRAW){
            reward = 0.0;
        }
        else
            reward = 0.0;
        
        Status status = new Status(board.getBoardValues());
        learner.memorize(status, reward);
        learner.update();
        
        if(result != GameStatus.RUN)
            learner.endEpisode();
    }

    @Override
    public int nextMove(Board board) {
        return learner.next(new Status(board.getBoardValues()));
    }

    @Override
    public void save() {
        learner.save();
    }

    @Override
    public void load() {
        learner.load();
    }
}