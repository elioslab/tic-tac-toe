package elios.tictactoe.agents;

import elios.tictactoe.Board;
import elios.tictactoe.Players;
import elios.tictactoe.GameStatus;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;

public class HumanAgent implements Agent
{
    Scanner scan; 

    public HumanAgent() 
    {
        scan = new Scanner(System.in);
    }
    
    
    @Override
    public void init() 
    {
    
    }

    @Override
    public void update(GameStatus result, Board board) 
    {
        
    }

    @Override
    public int nextMove(Board board) 
    {
        board.print();
        System.out.println();
        System.out.print("Select a move: ");
       
        int move = -1;
        
        boolean valid = false;
        while(!valid)
        {
            try
            {
                move = scan.nextInt();
                if(move<0 || move>8)
                    System.out.print("Invalid, select a move: ");
                else if (board.getValue(move) != Players.EMPTY)
                    System.out.print("Invalid, select a move: ");
                else
                    valid = true;
            }
            catch(java.util.InputMismatchException e)
            {
                scan.next();
                System.out.print("Invalid, select a move: ");
            }
        }

        return move;
    }

    @Override
    public void save() {

    }

    @Override
    public void load() {

    }
}
