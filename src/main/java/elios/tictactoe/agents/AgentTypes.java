package elios.tictactoe.agents;

public enum AgentTypes { RANDOM, EXPERT_CENTER, EXPERT_CORNER, EXPERT_WIN, TREE, CRITIC, HUMAN, CRITIC_LOAD, QLEARN }
