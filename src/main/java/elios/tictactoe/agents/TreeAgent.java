package elios.tictactoe.agents;

import elios.tictactoe.Board;
import elios.tictactoe.Players;
import elios.tictactoe.GameStatus;
import elios.tictactoe.agents.Agent;
import elios.tictactoe.tree.Tree;

public class TreeAgent implements Agent
{
    private Tree tree;
    private final Players player;

    public TreeAgent(Players player) 
    {
        this.player = player;
    }
    
    @Override
    public void init() 
    {

    }

    @Override
    public int nextMove(Board board) 
    {
        tree = new Tree(board, player, -1);
        tree.propagateUtility();

        return tree.getBestChild().getPosition();
    }

    @Override
    public void update(GameStatus result, Board board) {
    }

    @Override
    public void save() {

    }

    @Override
    public void load() {

    }
}
