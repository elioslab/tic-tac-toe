package elios.tictactoe.agents;

import elios.tictactoe.Board;
import elios.tictactoe.Players;
import elios.tictactoe.GameStatus;

public class ExpertAgent implements Agent
{
    private final Players player;
    private final Players opponent;
    private final ExpertAgentTypes type;

    public ExpertAgent(Players player, ExpertAgentTypes type) 
    {
        this.type = type;
        this.player = player;
        
        if(player == Players.MAX)
            opponent = Players.MIN;
        else
            opponent = Players.MAX;
    }
    
    public void init()
    {
        
    }
    
    public int nextMove(Board board)
    {
        if(type == ExpertAgentTypes.CENTER) return tryWinNotLoseStartCenter(board);
        else if (type == ExpertAgentTypes.CORNER) return tryWinNotLoseStartCorner(board);
        else if (type == ExpertAgentTypes.WIN) return tryWin(board);
        else throw(new IndexOutOfBoundsException("Expert type =" + type + " unknown"));
    }

    private int tryWinNotLoseStartCorner(Board board)
    {
        
	// All'inizio metti un segno nell'angolo
	if (board.getValue(2) == Players.EMPTY)
		return 2;

	// se puoi fare un tris, allora fallo
	if ((board.getValue(0) == Players.EMPTY) &&
	    ( (board.getValue(1) == player && board.getValue(2) == player) ||
	      (board.getValue(4) == player && board.getValue(8) == player) ||
		  (board.getValue(3) == player && board.getValue(6) == player)))
		  return 0;
	if ((board.getValue(1) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(2) == player) ||
	      (board.getValue(4) == player && board.getValue(7) == player)))
		  return 1;
	if ((board.getValue(2) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(1) == player) ||
	      (board.getValue(4) == player && board.getValue(6) == player) ||
		  (board.getValue(5) == player && board.getValue(8) == player)))
		  return 2;
        if ((board.getValue(3) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(6) == player) ||
	      (board.getValue(4) == player && board.getValue(5) == player)))
		  return 3;
	if ((board.getValue(4) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(8) == player) ||
	      (board.getValue(1) == player && board.getValue(7) == player) ||
		  (board.getValue(2) == player && board.getValue(6) == player) ||
		  (board.getValue(3) == player && board.getValue(5) == player)))
		  return 4;
	if ((board.getValue(5) == Players.EMPTY) &&
	    ( (board.getValue(2) == player && board.getValue(8) == player) ||
	      (board.getValue(3) == player && board.getValue(4) == player)))
		  return 5;
	if ((board.getValue(6) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(3) == player) ||
	      (board.getValue(2) == player && board.getValue(4) == player) ||
		  (board.getValue(7) == player && board.getValue(8) == player)))
		  return 6;
	if ((board.getValue(7) == Players.EMPTY) &&
	    ( (board.getValue(1) == player && board.getValue(4) == player) ||
	      (board.getValue(6) == player && board.getValue(8) == player)))
		  return 7;
	if ((board.getValue(8) == Players.EMPTY) &&
	    ( (board.getValue(2) == player && board.getValue(5) == player) ||
	      (board.getValue(0) == player && board.getValue(4) == player) ||
		  (board.getValue(6) == player && board.getValue(7) == player)))
		  return 8;

	// se l'avversaro ha un potenziale tris, allora bloccalo
	if ((board.getValue(0) == Players.EMPTY) &&
	    ( (board.getValue(1) == opponent && board.getValue(2) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(8) == opponent) ||
		  (board.getValue(3) == opponent && board.getValue(6) == opponent)))
		  return 0;
	if ((board.getValue(1) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(2) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(7) == opponent)))
		  return 1;
	if ((board.getValue(2) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(1) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(6) == opponent) ||
		  (board.getValue(5) == opponent && board.getValue(8) == opponent)))
		  return 2;
        if ((board.getValue(3) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(6) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(5) == opponent)))
		  return 3;
	if ((board.getValue(4) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(8) == opponent) ||
	      (board.getValue(1) == opponent && board.getValue(7) == opponent) ||
		  (board.getValue(2) == opponent && board.getValue(6) == opponent) ||
		  (board.getValue(3) == opponent && board.getValue(5) == opponent)))
		  return 4;
	if ((board.getValue(5) == Players.EMPTY) &&
	    ( (board.getValue(2) == opponent && board.getValue(8) == opponent) ||
	      (board.getValue(3) == opponent && board.getValue(4) == opponent)))
		  return 5;
	if ((board.getValue(6) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(3) == opponent) ||
	      (board.getValue(2) == opponent && board.getValue(4) == opponent) ||
		  (board.getValue(7) == opponent && board.getValue(8) == opponent)))
		  return 6;
	if ((board.getValue(7) == Players.EMPTY) &&
	    ( (board.getValue(1) == opponent && board.getValue(4) == opponent) ||
	      (board.getValue(6) == opponent && board.getValue(8) == opponent)))
		  return 7;
	if ((board.getValue(8) == Players.EMPTY) &&
	    ( (board.getValue(2) == opponent && board.getValue(5) == opponent) ||
	      (board.getValue(0) == opponent && board.getValue(4) == opponent) ||
		  (board.getValue(6) == opponent && board.getValue(7) == opponent)))
		  return 8;
	
	// se puoi fare una doppiacoppia allora falla 
	if( (board.getValue(4) == Players.EMPTY) &&
		((((board.getValue(0) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(1) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == player))) ||
		(((board.getValue(0) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(2) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(0) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(3) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == player))) ||
		(((board.getValue(1) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == player)) &&
		((board.getValue(2) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(1) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == player)) &&
		((board.getValue(3) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == player))) ||
		(((board.getValue(2) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == player)) &&
		((board.getValue(3) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == player)))))
		return 4;
	if( (board.getValue(0) == Players.EMPTY) &&
		((((board.getValue(1) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(3) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(1) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(4) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == player))) ||
		(((board.getValue(4) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(3) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(6) == player)))))
		return 0;
	if( (board.getValue(2) == Players.EMPTY) &&
		((((board.getValue(0) == player && board.getValue(1) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(1) == player)) &&
		((board.getValue(4) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(0) == player && board.getValue(1) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(1) == player)) &&
		((board.getValue(5) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == player))) ||
		(((board.getValue(5) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(4) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == player)))))
		return 2;
	if( (board.getValue(6) == Players.EMPTY) &&
		((((board.getValue(0) == player && board.getValue(3) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(3) == player)) &&
		((board.getValue(7) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == player))) ||
		(((board.getValue(0) == player && board.getValue(3) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(3) == player)) &&
		((board.getValue(4) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(2) == player))) ||
		(((board.getValue(4) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(7) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == player)))))
		return 6;
	if( (board.getValue(8) == Players.EMPTY) &&
		((((board.getValue(5) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(0) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == player))) ||
		(((board.getValue(5) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(6) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == player))) ||
		(((board.getValue(0) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == player)) &&
		((board.getValue(6) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == player)))))
		return 8;
	if( (board.getValue(1) == Players.EMPTY) &&
		(((board.getValue(0) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(4) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(7) == player))))
		return 1;
	if( (board.getValue(3) == Players.EMPTY) &&
		(((board.getValue(0) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(6) == player)) &&
		((board.getValue(4) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(5) == player))))
		return 3;
	if( (board.getValue(5) == Players.EMPTY) &&
		(((board.getValue(2) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(3) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(4) == player))))
		return 5;
	if( (board.getValue(7) == Players.EMPTY) &&
		(((board.getValue(1) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(4) == player)) &&
		((board.getValue(6) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(8) == player))))
		return 7;
	
	//se il centro non e' stato occupato e se e' stata occupata una casella di lato, allora occupa il centro
	if(board.getValue(4) == Players.EMPTY &&
	  (board.getValue(1) == opponent || board.getValue(3) == opponent || board.getValue(5) == opponent || board.getValue(7) == opponent))
	  return 4;

	//se il centro non e' stato occupato e se e' stata occupata una casella di spigolo non opposta, 
	// allora occupa lo spigolo opposto
	if(board.getValue(4) == Players.EMPTY &&
	   board.getValue(6) == Players.EMPTY &&	
	  (board.getValue(0) == opponent || board.getValue(8) == opponent))
	  return 6;

	//se il centro non e' stato occupato e se e' stata occupata La casella di spigolo opposta, 
	// allora occupa uno spigolo adiacente
	if(board.getValue(4) == Players.EMPTY &&
	   board.getValue(6) == opponent &&
	   board.getValue(0) == Players.EMPTY)
	   return 0;
	if(board.getValue(4) == Players.EMPTY &&
	   board.getValue(6) == opponent &&
	   board.getValue(8) == Players.EMPTY)
	   return 8;

	// prova a fare una coppia
	if(board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 5;
	if(board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY)
		return 1;

	// mettiti in una posizione libera
	for(int i=0; i<board.getSize()*board.getSize(); i++)
            if(board.getValue(i) == Players.EMPTY)
		return i;
        
        return -1;
    }

    private int tryWinNotLoseStartCenter(Board board)
    {
	//metti un segno al centro
	if (board.getValue(4) == Players.EMPTY)
		return 4;

	// se puoi fare un tris, allora fallo
	if ((board.getValue(0) == Players.EMPTY) &&
	    ( (board.getValue(1) == player && board.getValue(2) == player) ||
	      (board.getValue(4) == player && board.getValue(8) == player) ||
		  (board.getValue(3) == player && board.getValue(6) == player)))
		  return 0;
	if ((board.getValue(1) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(2) == player) ||
	      (board.getValue(4) == player && board.getValue(7) == player)))
		  return 1;
	if ((board.getValue(2) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(1) == player) ||
	      (board.getValue(4) == player && board.getValue(6) == player) ||
		  (board.getValue(5) == player && board.getValue(8) == player)))
		  return 2;
        if ((board.getValue(3) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(6) == player) ||
	      (board.getValue(4) == player && board.getValue(5) == player)))
		  return 3;
	if ((board.getValue(4) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(8) == player) ||
	      (board.getValue(1) == player && board.getValue(7) == player) ||
		  (board.getValue(2) == player && board.getValue(6) == player) ||
		  (board.getValue(3) == player && board.getValue(5) == player)))
		  return 4;
	if ((board.getValue(5) == Players.EMPTY) &&
	    ( (board.getValue(2) == player && board.getValue(8) == player) ||
	      (board.getValue(3) == player && board.getValue(4) == player)))
		  return 5;
	if ((board.getValue(6) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(3) == player) ||
	      (board.getValue(2) == player && board.getValue(4) == player) ||
		  (board.getValue(7) == player && board.getValue(8) == player)))
		  return 6;
	if ((board.getValue(7) == Players.EMPTY) &&
	    ( (board.getValue(1) == player && board.getValue(4) == player) ||
	      (board.getValue(6) == player && board.getValue(8) == player)))
		  return 7;
	if ((board.getValue(8) == Players.EMPTY) &&
	    ( (board.getValue(2) == player && board.getValue(5) == player) ||
	      (board.getValue(0) == player && board.getValue(4) == player) ||
		  (board.getValue(6) == player && board.getValue(7) == player)))
		  return 8;

	// se l'avversaro ha un potenziale tris, allora bloccalo
	if ((board.getValue(0) == Players.EMPTY) &&
	    ( (board.getValue(1) == opponent && board.getValue(2) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(8) == opponent) ||
		  (board.getValue(3) == opponent && board.getValue(6) == opponent)))
		  return 0;
	if ((board.getValue(1) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(2) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(7) == opponent)))
		  return 1;
	if ((board.getValue(2) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(1) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(6) == opponent) ||
		  (board.getValue(5) == opponent && board.getValue(8) == opponent)))
		  return 2;
        if ((board.getValue(3) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(6) == opponent) ||
	      (board.getValue(4) == opponent && board.getValue(5) == opponent)))
		  return 3;
	if ((board.getValue(4) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(8) == opponent) ||
	      (board.getValue(1) == opponent && board.getValue(7) == opponent) ||
		  (board.getValue(2) == opponent && board.getValue(6) == opponent) ||
		  (board.getValue(3) == opponent && board.getValue(5) == opponent)))
		  return 4;
	if ((board.getValue(5) == Players.EMPTY) &&
	    ( (board.getValue(2) == opponent && board.getValue(8) == opponent) ||
	      (board.getValue(3) == opponent && board.getValue(4) == opponent)))
		  return 5;
	if ((board.getValue(6) == Players.EMPTY) &&
	    ( (board.getValue(0) == opponent && board.getValue(3) == opponent) ||
	      (board.getValue(2) == opponent && board.getValue(4) == opponent) ||
		  (board.getValue(7) == opponent && board.getValue(8) == opponent)))
		  return 6;
	if ((board.getValue(7) == Players.EMPTY) &&
	    ( (board.getValue(1) == opponent && board.getValue(4) == opponent) ||
	      (board.getValue(6) == opponent && board.getValue(8) == opponent)))
		  return 7;
	if ((board.getValue(8) == Players.EMPTY) &&
	    ( (board.getValue(2) == opponent && board.getValue(5) == opponent) ||
	      (board.getValue(0) == opponent && board.getValue(4) == opponent) ||
		  (board.getValue(6) == opponent && board.getValue(7) == opponent)))
		  return 8;

	// se l'avversarion ha una potenziale "doppiadoppiacoppia", allora bloccala
	if (board.getValue(0) == opponent &&
		board.getValue(1) == Players.EMPTY			 &&
		board.getValue(2) == Players.EMPTY			 &&
		board.getValue(3) == Players.EMPTY			 &&
		board.getValue(4) == player                              &&
		board.getValue(5) == Players.EMPTY			 &&
		board.getValue(6) == Players.EMPTY			 &&
		board.getValue(7) == Players.EMPTY			 &&
		board.getValue(8) == opponent)
		return 1;
	if (board.getValue(0) == Players.EMPTY                           &&
		board.getValue(1) == Players.EMPTY			 &&
		board.getValue(2) == opponent                            &&
		board.getValue(3) == Players.EMPTY			 &&
		board.getValue(4) == player		 &&
		board.getValue(5) == Players.EMPTY			 &&
		board.getValue(6) == opponent &&
		board.getValue(7) == Players.EMPTY			 &&
		board.getValue(8) == Players.EMPTY)
		return 1;

	// se l'avversario ha una potenziale "doppiacoppia", allora bloccala
	if( (board.getValue(4) == Players.EMPTY) &&
		((((board.getValue(0) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == opponent)) &&
		((board.getValue(1) == opponent && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == opponent))) ||
		(((board.getValue(0) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == opponent)) &&
		((board.getValue(2) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == opponent))) ||
		(((board.getValue(0) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == opponent)) &&
		((board.getValue(3) == opponent && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == opponent))) ||
		(((board.getValue(1) == opponent && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == opponent)) &&
		((board.getValue(2) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == opponent))) ||
		(((board.getValue(1) == opponent && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == opponent)) &&
		((board.getValue(3) == opponent && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == opponent))) ||
		(((board.getValue(2) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == opponent)) &&
		((board.getValue(3) == opponent && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == opponent)))))
		return 4;
	if( (board.getValue(0) == Players.EMPTY) &&
		((((board.getValue(1) == opponent && board.getValue(2) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(2) == opponent)) &&
		((board.getValue(3) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(6) == opponent))) ||
		(((board.getValue(1) == opponent && board.getValue(2) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(2) == opponent)) &&
		((board.getValue(4) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == opponent))) ||
		(((board.getValue(4) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == opponent)) &&
		((board.getValue(3) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(6) == opponent)))))
		return 0;
	if( (board.getValue(2) == Players.EMPTY) &&
		((((board.getValue(0) == opponent && board.getValue(1) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(1) == opponent)) &&
		((board.getValue(4) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == opponent))) ||
		(((board.getValue(0) == opponent && board.getValue(1) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(1) == opponent)) &&
		((board.getValue(5) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == opponent))) ||
		(((board.getValue(5) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == opponent)) &&
		((board.getValue(4) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == opponent)))))
		return 2;
	if( (board.getValue(6) == Players.EMPTY) &&
		((((board.getValue(0) == opponent && board.getValue(3) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(3) == opponent)) &&
		((board.getValue(7) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == opponent))) ||
		(((board.getValue(0) == opponent && board.getValue(3) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(3) == opponent)) &&
		((board.getValue(4) == opponent && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(2) == opponent))) ||
		(((board.getValue(4) == opponent && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(2) == opponent)) &&
		((board.getValue(7) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == opponent)))))
		return 6;
	if( (board.getValue(8) == Players.EMPTY) &&
		((((board.getValue(5) == opponent && board.getValue(2) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(2) == opponent)) &&
		((board.getValue(0) == opponent && board.getValue(4) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == opponent))) ||
		(((board.getValue(5) == opponent && board.getValue(2) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(2) == opponent)) &&
		((board.getValue(6) == opponent && board.getValue(7) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == opponent))) ||
		(((board.getValue(0) == opponent && board.getValue(4) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == opponent)) &&
		((board.getValue(6) == opponent && board.getValue(7) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == opponent)))))
		return 8;
	if( (board.getValue(1) == Players.EMPTY) &&
		(((board.getValue(0) == opponent && board.getValue(2) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(2) == opponent)) &&
		((board.getValue(4) == opponent && board.getValue(7) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(7) == opponent))))
		return 1;
	if( (board.getValue(3) == Players.EMPTY) &&
		(((board.getValue(0) == opponent && board.getValue(6) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(6) == opponent)) &&
		((board.getValue(4) == opponent && board.getValue(5) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(5) == opponent))))
		return 3;
	if( (board.getValue(5) == Players.EMPTY) &&
		(((board.getValue(2) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(8) == opponent)) &&
		((board.getValue(3) == opponent && board.getValue(4) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(4) == opponent))))
		return 5;
	if( (board.getValue(7) == Players.EMPTY) &&
		(((board.getValue(1) == opponent && board.getValue(4) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(4) == opponent)) &&
		((board.getValue(6) == opponent && board.getValue(8) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(8) == opponent))))
		return 7;
	
	//se hai una diagonale "libera" (i due corner opposti vuoti), allora occupala,
	//ma dal lato con un possibile tris
	if (board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{
		if( (board.getValue(1) == player) && (board.getValue(2) == Players.EMPTY) ||
			(board.getValue(1) == Players.EMPTY) && (board.getValue(2) == player) ||
			(board.getValue(3) == player) && (board.getValue(6) == Players.EMPTY) ||
			(board.getValue(3) == Players.EMPTY) && (board.getValue(6) == player))
			return 0;

		if( (board.getValue(2) == player) && (board.getValue(5) == Players.EMPTY) ||
			(board.getValue(2) == Players.EMPTY) && (board.getValue(5) == player) ||
			(board.getValue(6) == player) && (board.getValue(7) == Players.EMPTY) ||
			(board.getValue(6) == Players.EMPTY) && (board.getValue(7) == player))
			return 8;
	}
	if (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
	{
		if( (board.getValue(0) == player) && (board.getValue(1) == Players.EMPTY) ||
			(board.getValue(0) == Players.EMPTY) && (board.getValue(1) == player) ||
			(board.getValue(5) == player) && (board.getValue(8) == Players.EMPTY) ||
			(board.getValue(5) == Players.EMPTY) && (board.getValue(8) == player))
			return 2;

		if( (board.getValue(0) == player) && (board.getValue(3) == Players.EMPTY) ||
			(board.getValue(0) == Players.EMPTY) && (board.getValue(3) == player) ||
			(board.getValue(7) == player) && (board.getValue(8) == Players.EMPTY) ||
			(board.getValue(7) == Players.EMPTY) && (board.getValue(8) == player))
			return 6;
	}

	//se hai una diagonale "libera" (i due corner opposti vuoti), allora occupala
	//dalla parte migliore
	if (board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{
		if( (board.getValue(1) == Players.EMPTY) && (board.getValue(3) == Players.EMPTY) )
			return 0;
		if( (board.getValue(5) == Players.EMPTY) && (board.getValue(7) == Players.EMPTY) )
			return 8;
	}
	if (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
	{
		if( (board.getValue(1) == Players.EMPTY) && (board.getValue(5) == Players.EMPTY) )
			return 2;
		if( (board.getValue(7) == Players.EMPTY) && (board.getValue(3) == Players.EMPTY) )
			return 8;
	}

	//se hai una diagonale "libera" (i due corner opposti vuoti), allora occupala
	if (board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 0;
	if (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
		return 2;

	//mettilo in una casella laterale libera
	if (board.getValue(1) == Players.EMPTY)
		return 1;
	if (board.getValue(3) == Players.EMPTY)
		return 3;
	if (board.getValue(5) == Players.EMPTY)
		return 5;
	if (board.getValue(7) == Players.EMPTY)
		return 7;
        
        // mettiti in una posizione libera
	for(int i=0; i<board.getSize()*board.getSize(); i++)
            if(board.getValue(i) == Players.EMPTY)
		return i;
        
        return -1;
    }

    private int tryWin(Board board)
    {
	// metti un segno in alto a sinistra
	if (board.getValue(2) == Players.EMPTY)
		return 2;

	// se puoi fare un tris, allora fallo
	if ((board.getValue(0) == Players.EMPTY) &&
	    ( (board.getValue(1) == player && board.getValue(2) == player) ||
	      (board.getValue(4) == player && board.getValue(8) == player) ||
		  (board.getValue(3) == player && board.getValue(6) == player)))
		  return 0;
	if ((board.getValue(1) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(2) == player) ||
	      (board.getValue(4) == player && board.getValue(7) == player)))
		  return 1;
	if ((board.getValue(2) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(1) == player) ||
	      (board.getValue(4) == player && board.getValue(6) == player) ||
		  (board.getValue(5) == player && board.getValue(8) == player)))
		  return 2;
    if ((board.getValue(3) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(6) == player) ||
	      (board.getValue(4) == player && board.getValue(5) == player)))
		  return 3;
	if ((board.getValue(4) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(8) == player) ||
	      (board.getValue(1) == player && board.getValue(7) == player) ||
		  (board.getValue(2) == player && board.getValue(6) == player) ||
		  (board.getValue(3) == player && board.getValue(5) == player)))
		  return 4;
	if ((board.getValue(5) == Players.EMPTY) &&
	    ( (board.getValue(2) == player && board.getValue(8) == player) ||
	      (board.getValue(3) == player && board.getValue(4) == player)))
		  return 5;
	if ((board.getValue(6) == Players.EMPTY) &&
	    ( (board.getValue(0) == player && board.getValue(3) == player) ||
	      (board.getValue(2) == player && board.getValue(4) == player) ||
		  (board.getValue(7) == player && board.getValue(8) == player)))
		  return 6;
	if ((board.getValue(7) == Players.EMPTY) &&
	    ( (board.getValue(1) == player && board.getValue(4) == player) ||
	      (board.getValue(6) == player && board.getValue(8) == player)))
		  return 7;
	if ((board.getValue(8) == Players.EMPTY) &&
	    ( (board.getValue(2) == player && board.getValue(5) == player) ||
	      (board.getValue(0) == player && board.getValue(4) == player) ||
		  (board.getValue(6) == player && board.getValue(7) == player)))
		  return 8;

	// se puoi fare una "doppiacoppia", allora preparala
	if( (board.getValue(4) == Players.EMPTY) &&
		((((board.getValue(0) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(1) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == player))) ||
		(((board.getValue(0) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(2) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(0) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(3) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == player))) ||
		(((board.getValue(1) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == player)) &&
		((board.getValue(2) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(1) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == player)) &&
		((board.getValue(3) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == player))) ||
		(((board.getValue(2) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == player)) &&
		((board.getValue(3) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == player)))))
		return 4;
	if( (board.getValue(0) == Players.EMPTY) &&
		((((board.getValue(1) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(3) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(1) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(4) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == player))) ||
		(((board.getValue(4) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(3) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(6) == player)))))
		return 0;
	if( (board.getValue(2) == Players.EMPTY) &&
		((((board.getValue(0) == player && board.getValue(1) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(1) == player)) &&
		((board.getValue(4) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == player))) ||
		(((board.getValue(0) == player && board.getValue(1) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(1) == player)) &&
		((board.getValue(5) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == player))) ||
		(((board.getValue(5) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(4) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == player)))))
		return 2;
	if( (board.getValue(6) == Players.EMPTY) &&
		((((board.getValue(0) == player && board.getValue(3) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(3) == player)) &&
		((board.getValue(7) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == player))) ||
		(((board.getValue(0) == player && board.getValue(3) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(3) == player)) &&
		((board.getValue(4) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(2) == player))) ||
		(((board.getValue(4) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(7) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == player)))))
		return 6;
	if( (board.getValue(8) == Players.EMPTY) &&
		((((board.getValue(5) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(0) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == player))) ||
		(((board.getValue(5) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(6) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == player))) ||
		(((board.getValue(0) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == player)) &&
		((board.getValue(6) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == player)))))
		return 8;
	if( (board.getValue(1) == Players.EMPTY) &&
		(((board.getValue(0) == player && board.getValue(2) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(2) == player)) &&
		((board.getValue(4) == player && board.getValue(7) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(7) == player))))
		return 1;
	if( (board.getValue(3) == Players.EMPTY) &&
		(((board.getValue(0) == player && board.getValue(6) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(6) == player)) &&
		((board.getValue(4) == player && board.getValue(5) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(5) == player))))
		return 3;
	if( (board.getValue(5) == Players.EMPTY) &&
		(((board.getValue(2) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(8) == player)) &&
		((board.getValue(3) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(4) == player))))
		return 5;
	if( (board.getValue(7) == Players.EMPTY) &&
		(((board.getValue(1) == player && board.getValue(4) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(4) == player)) &&
		((board.getValue(6) == player && board.getValue(8) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(8) == player))))
		return 7;
	
	// se puoi preparare una "coppia", allora preparala, ma da una parte che apra una possibilita'
	if (board.getValue(0) == player && board.getValue(1) == Players.EMPTY && board.getValue(2) == Players.EMPTY)
	{
		if (board.getValue(4) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
			return 1;
		if ((board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return 2;
	}
	if (board.getValue(0) == player && board.getValue(4) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{
		if ((board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return  4;
		if ((board.getValue(2) == Players.EMPTY && board.getValue(5) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == Players.EMPTY))
			return 8;
	}
	if (board.getValue(0) == player && board.getValue(3) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
	{
		if (board.getValue(4) == Players.EMPTY && board.getValue(5) == Players.EMPTY)	
			return  3;
		if ((board.getValue(7) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(4) == Players.EMPTY))
			return 6;
	}
	if (board.getValue(1) == player && board.getValue(0) == Players.EMPTY && board.getValue(2) == Players.EMPTY)
	{		
		if ((board.getValue(3) == Players.EMPTY && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 0;
		if ((board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return 2;
	}
	if (board.getValue(1) == player && board.getValue(4) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
	{
		if ((board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == Players.EMPTY))
			return  4;
		if (board.getValue(6) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
			return 7;
	}
	if (board.getValue(2) == player && board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY)
	{		
		if ((board.getValue(3) == Players.EMPTY && board.getValue(6) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 0;
		if (board.getValue(4) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
			return 1;
	}
	if (board.getValue(2) == player && board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{		
		if (board.getValue(3) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
			return 5;
		if ((board.getValue(0) == Players.EMPTY && board.getValue(4) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == Players.EMPTY))
			return 8;
	}
	if (board.getValue(2) == player && board.getValue(4) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
	{		
		if ((board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == Players.EMPTY))
			return 4;
		if ((board.getValue(0) == Players.EMPTY && board.getValue(3) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 6;
	}
	if (board.getValue(3) == player && board.getValue(4) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
	{		
		if ((board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY))
			return 4;
		if (board.getValue(2) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
			return 5;
	}
	if (board.getValue(3) == player && board.getValue(0) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
	{		
		if ((board.getValue(1) == Players.EMPTY && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 0;
		if ((board.getValue(4) == Players.EMPTY && board.getValue(2) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 6;
	}
	if (board.getValue(4) == player && board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
	{		
		if (board.getValue(0) == Players.EMPTY && board.getValue(2) == Players.EMPTY)
			return 1;
		if (board.getValue(6) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
			return 7;
	}
	if (board.getValue(4) == player && board.getValue(3) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
	{		
		if (board.getValue(0) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
			return 3;
		if (board.getValue(2) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
			return 5;
	}
	if (board.getValue(4) == player && board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{		
		if ((board.getValue(3) == Players.EMPTY && board.getValue(6) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(2) == Players.EMPTY))
			return 0;
		if ((board.getValue(5) == Players.EMPTY && board.getValue(2) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return 8;
	}
	if (board.getValue(4) == player && board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
	{		
		if ((board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 2;
		if ((board.getValue(0) == Players.EMPTY && board.getValue(3) == Players.EMPTY) || (board.getValue(7) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 6;
	}
	if (board.getValue(5) == player && board.getValue(3) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
	{		
		if (board.getValue(0) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
			return 3;
		if ((board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY))
			return 4;
	}
	if (board.getValue(5) == player && board.getValue(2) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{		
		if ((board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return 2;
		if ((board.getValue(0) == Players.EMPTY && board.getValue(4) == Players.EMPTY) || (board.getValue(6) == Players.EMPTY && board.getValue(7) == Players.EMPTY))
			return 8;
	}
	if (board.getValue(6) == player && board.getValue(7) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{		
		if (board.getValue(1) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
			return 7;
		if ((board.getValue(2) == Players.EMPTY && board.getValue(5) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == Players.EMPTY))
			return 8;
	}
	if (board.getValue(6) == player && board.getValue(0) == Players.EMPTY && board.getValue(3) == Players.EMPTY)
	{		
		if ((board.getValue(1) == Players.EMPTY && board.getValue(2) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 0;
		if (board.getValue(4) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
			return 3;
	}
	if (board.getValue(6) == player && board.getValue(2) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
	{		
		if ((board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY) || (board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY))
			return 2;
		if ((board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == Players.EMPTY) )
			return 4;
	}
	if (board.getValue(7) == player && board.getValue(1) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
	{		
		if (board.getValue(0) == Players.EMPTY && board.getValue(2) == Players.EMPTY)
			return 1;
		if ((board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(5) == Players.EMPTY))
			return 4;
	}
	if (board.getValue(7) == player && board.getValue(6) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
	{		
		if ((board.getValue(0) == Players.EMPTY && board.getValue(3) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(4) == Players.EMPTY))
			return 6;
		if ((board.getValue(2) == Players.EMPTY && board.getValue(5) == Players.EMPTY) || (board.getValue(0) == Players.EMPTY && board.getValue(4) == Players.EMPTY))
			return 8;
	}
	if (board.getValue(8) == player && board.getValue(2) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
	{		
		if ((board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY) || (board.getValue(4) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return 2;
		if (board.getValue(3) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
			return 5;
	}
	if (board.getValue(8) == player && board.getValue(6) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
	{		
		if ((board.getValue(0) == Players.EMPTY && board.getValue(3) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(4) == Players.EMPTY))
			return 6;
		if (board.getValue(1) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
			return 7;
	}
	if (board.getValue(8) == player && board.getValue(0) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
	{		
		if ((board.getValue(1) == Players.EMPTY && board.getValue(2) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return 0;
		if ((board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY) || (board.getValue(3) == Players.EMPTY && board.getValue(4) == Players.EMPTY) || (board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY))
			return 4;
	}

	// se puoi preparare una "coppia", allora preparala
	if (board.getValue(0) == player && board.getValue(1) == Players.EMPTY && board.getValue(2) == Players.EMPTY)
		return 1;
	if (board.getValue(0) == player && board.getValue(4) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return  4;
	if (board.getValue(0) == player && board.getValue(3) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
		return  3;
	if (board.getValue(1) == player && board.getValue(0) == Players.EMPTY && board.getValue(2) == Players.EMPTY)
		return 0;
	if (board.getValue(1) == player && board.getValue(4) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
		return 4;
	if (board.getValue(2) == player && board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY)
		return 0;
	if (board.getValue(2) == player && board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 5;
	if (board.getValue(2) == player && board.getValue(4) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
		return 4;
	if (board.getValue(3) == player && board.getValue(4) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
		return 4;
	if (board.getValue(3) == player && board.getValue(0) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
		return 0;
	if (board.getValue(4) == player && board.getValue(1) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
		return 1;
	if (board.getValue(4) == player && board.getValue(3) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
		return 3;
	if (board.getValue(4) == player && board.getValue(0) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 0;
	if (board.getValue(4) == player && board.getValue(2) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
		return 2;
	if (board.getValue(5) == player && board.getValue(3) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
		return 3;
	if (board.getValue(5) == player && board.getValue(2) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 2;
	if (board.getValue(6) == player && board.getValue(7) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 7;
	if (board.getValue(6) == player && board.getValue(0) == Players.EMPTY && board.getValue(3) == Players.EMPTY)
		return 0;
	if (board.getValue(6) == player && board.getValue(2) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
		return 2;
	if (board.getValue(7) == player && board.getValue(1) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
		return 1;
	if (board.getValue(7) == player && board.getValue(6) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 6;
	if (board.getValue(8) == player && board.getValue(2) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
		return 2;
	if (board.getValue(8) == player && board.getValue(6) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
		return 6;
	if (board.getValue(8) == player && board.getValue(0) == Players.EMPTY && board.getValue(4) == Players.EMPTY)
		return 0;

	//se esiste un terno vuoto, allora inizialo
	if (board.getValue(0) == Players.EMPTY && board.getValue(1) == Players.EMPTY && board.getValue(2) == Players.EMPTY)
		return 0;
	if (board.getValue(0) == Players.EMPTY && board.getValue(4) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 0;
	if (board.getValue(0) == Players.EMPTY && board.getValue(3) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
		return 0;
	if (board.getValue(1) == Players.EMPTY && board.getValue(4) == Players.EMPTY && board.getValue(7) == Players.EMPTY)
		return 1;
	if (board.getValue(2) == Players.EMPTY && board.getValue(5) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 2;
	if (board.getValue(2) == Players.EMPTY && board.getValue(4) == Players.EMPTY && board.getValue(6) == Players.EMPTY)
		return 2;
	if (board.getValue(3) == Players.EMPTY && board.getValue(4) == Players.EMPTY && board.getValue(5) == Players.EMPTY)
		return 3;
	if (board.getValue(6) == Players.EMPTY && board.getValue(7) == Players.EMPTY && board.getValue(8) == Players.EMPTY)
		return 6;

	//mettilo in una casella libera
	if (board.getValue(0) == Players.EMPTY)
		return 0;
	if (board.getValue(1) == Players.EMPTY)
		return 1;
	if (board.getValue(2) == Players.EMPTY)
		return 2;
	if (board.getValue(3) == Players.EMPTY)
		return 3;
	if (board.getValue(4) == Players.EMPTY)
		return 4;
	if (board.getValue(5) == Players.EMPTY)
		return 5;
	if (board.getValue(6) == Players.EMPTY)
		return 6;
	if (board.getValue(7) == Players.EMPTY)
		return 7;
    
        return -1;
    }

    @Override
    public void update(GameStatus result, Board board) {
    }

    @Override
    public void save() {

    }

    @Override
    public void load() {

    }
}
