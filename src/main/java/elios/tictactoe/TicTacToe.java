package elios.tictactoe;

import elios.tictactoe.agents.AgentTypes;
import elios.tictactoe.agents.Agent;

public class TicTacToe 
{
    private final Agent player;
    private final Agent opponent;
    private Board board;
    private final int boardsize;
    private final boolean verbose;
    private final BoardMemory memory;
    private GameStatus status;

    public TicTacToe(AgentTypes playertype, AgentTypes opponenttype, int boardsize, boolean verbose)  {
        this.player = AgentFactory.createPlayer(playertype, Players.MAX, boardsize);
        this.opponent = AgentFactory.createPlayer(opponenttype, Players.MIN, boardsize);
        this.boardsize = boardsize;
        this.verbose = verbose;
        this.memory = new BoardMemory();
    }
    
    public GameStatus run() {
        player.init();
        opponent.init();
        
        board = new Board(boardsize);
        memory.reset();
        
        GameStatus status = GameStatus.RUN;
        while(status == status.RUN) {
            
            Integer move = player.nextMove(board);
            status = check(board, move, Players.MAX);

            if(status == GameStatus.RUN) {
                move = opponent.nextMove(board);
                status = check(board, move, Players.MIN);
            }

            player.update(status, board);
            opponent.update(status, board);
        }
        
        board.setWinner(status);
        return status;
    }
    
    public void load() {
        player.load();
        opponent.load();
    }
    
    public void save() {
        player.save();
        opponent.save();
    }
    
    private GameStatus win(Players player) {
        if(player == Players.MAX)
            return GameStatus.MAX;
        else
            return GameStatus.MIN;
    }
    
    private GameStatus check(Board board, Integer move, Players player) {
        if(verbose) board.print();
        
        memory.add(board.duplicate());
        
        if(board.getValue(move) != Players.EMPTY)
            return GameStatus.ERROR;
        board.setValue(move, player);
        if(board.isFinalAround(move))
            return win(player);
        if(board.isFull())
            return GameStatus.DRAW;
        return GameStatus.RUN;
    }
}
