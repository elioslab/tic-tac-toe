package elios.tictactoe;

import elios.tictactoe.agents.*;
import elios.tictactoe.agents.critic.LearningAgent;
import elios.tictactoe.agents.RLAgent;
import elios.tictactoe.agents.TreeAgent;

public class AgentFactory 
{
    static Agent createPlayer(AgentTypes type, Players player, int boardsize)
    {
        if(type == AgentTypes.EXPERT_CENTER)
            return new ExpertAgent(player, ExpertAgentTypes.CENTER);
        if(type == AgentTypes.EXPERT_CORNER)
            return new ExpertAgent(player, ExpertAgentTypes.CORNER);
        if(type == AgentTypes.EXPERT_WIN)
            return new ExpertAgent(player, ExpertAgentTypes.WIN);
        if(type == AgentTypes.RANDOM)
            return new RandomAgent();
        if(type == AgentTypes.TREE)
            return new TreeAgent(player);
        if(type == AgentTypes.CRITIC)
            return new LearningAgent(player, boardsize, false);
        if(type == AgentTypes.CRITIC_LOAD)
            return new LearningAgent(player, boardsize, true);
        if(type == AgentTypes.HUMAN)
            return new HumanAgent();
        if(type == AgentTypes.QLEARN)
            return new RLAgent(player);
        
        throw(new IndexOutOfBoundsException("Type=" + type + " not found"));
    }
}
