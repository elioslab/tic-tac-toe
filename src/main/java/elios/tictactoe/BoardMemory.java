
package elios.tictactoe;

import java.util.ArrayList;
import java.util.List;

public class BoardMemory 
{
    private List<Board> memory;

    public BoardMemory() 
    {
        memory = new ArrayList<>();
    }
    
    public void add(Board board)
    {
        memory.add(board);
    }
    
    public Board get(int time)
    {
        return memory.get(time);
    }
    
    public void reset()
    {
        memory = new ArrayList<>();
    }
}
