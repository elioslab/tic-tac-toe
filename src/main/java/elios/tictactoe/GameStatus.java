package elios.tictactoe;

public enum GameStatus { MAX, MIN, DRAW, ERROR, RUN }
