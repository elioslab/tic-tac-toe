package elios.tictactoe;

import elios.tictactoe.agents.AgentTypes;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.LiveGraph.dataFile.write.DataStreamWriter;
import org.LiveGraph.dataFile.write.DataStreamWriterFactory;

public class Main 
{
    public static void main(String[] args) throws IOException
    {   
        // SIMULATION PARAMETERS
        
        int GameNumber = 20000000;
        int window = 100;
        int boardsize = 3;
        boolean verbose = false;
        AgentTypes player = AgentTypes.QLEARN;
        AgentTypes opponent = AgentTypes.RANDOM;
        
        // ********* //
        
        if(Files.exists(Paths.get("./stream.lgdat")))
            Files.delete(Paths.get("./stream.lgdat"));
        DataStreamWriter stream = DataStreamWriterFactory.createDataWriter("./", "stream");
        stream.setSeparator(";");
        stream.writeFileInfo("Performance File");
        stream.addDataSeries("max"); 
        stream.addDataSeries("min"); 
        stream.addDataSeries("draw"); 
        stream.addDataSeries("no");
        
        
        Statistics statistic = new Statistics();
        
        TicTacToe game = new TicTacToe(player, opponent, boardsize, verbose);
        
        game.load();
        
        for(int i=1; i<GameNumber; i++)
        {
            GameStatus result = game.run();
            
            //if(result == ResultTypes.MIN)
            //    System.out.print(i);
            
            statistic.add(result);
            
            //System.out.println(result);
            
            if(i%window == 0)
            {
                System.out.print("MAX="  + statistic.freq(GameStatus.MAX,  10000) * 100 + "% (" + statistic.number(GameStatus.MAX,  1000)  + ") ");
                System.out.print("MIN="  + statistic.freq(GameStatus.MIN,  10000) * 100 + "% (" + statistic.number(GameStatus.MIN,  1000)  + ") ");
                System.out.print("DRAW=" + statistic.freq(GameStatus.DRAW, 10000) * 100 + "% (" + statistic.number(GameStatus.DRAW, 1000) + ") ");
                System.out.print("NO="   + statistic.freq(GameStatus.ERROR, 10000) * 100 + "% (" + statistic.number(GameStatus.ERROR, 1000) + ") ");
                System.out.println();
                
                stream.setDataValue("max", statistic.freq(GameStatus.MAX,  10000) * 100);
                stream.setDataValue("min", statistic.freq(GameStatus.MIN,  10000) * 100);
                stream.setDataValue("draw", statistic.freq(GameStatus.DRAW, 10000) * 100);
                stream.setDataValue("no", statistic.freq(GameStatus.ERROR, 10000) * 100);
                stream.writeDataSet();
            }
        }

        stream.close();;
        game.save();
    }
}
