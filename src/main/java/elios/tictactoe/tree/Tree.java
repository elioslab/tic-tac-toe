package elios.tictactoe.tree;

import elios.tictactoe.Board;
import elios.tictactoe.Players;
import java.util.ArrayList;
import java.util.List;

public class Tree 
{
    private final Board board;
    private float utility;
    private final Players player;
    private final Players opponent;
    private final List<Tree> childs;
    private final int position;

    public Board getBoard() {
        return board;
    }

    public Players getPlayer() {
        return player;
    }

    public List<Tree> getChilds() {
        return childs;
    }

    public int getPosition() {
        return position;
    }

    public Tree(Board board, Players player, int position)
    {
	this.player = player;
        if(player == Players.MAX)
            opponent = Players.MIN;
        else
            opponent = Players.MAX;
        this.position = position;
        this.board = board;
        this.childs = new ArrayList<>();

        if(board.isFinalAround(position) == false)
        {
            for(int i=0; i<this.board.getSize() * this.board.getSize(); i++)
            {
                if(this.board.getValue(i) == Players.EMPTY)
                {
                     Board newboard = board.duplicate();
                     newboard.setValue(i, player);
                     childs.add(new Tree(newboard, opponent, i));
                }
            }
        }
    }
    
    public float propagateUtility()
    {
	int number = childs.size();
	
	if(board.isFinalAround(position))
        {
            if(player == Players.MAX)
                utility = -1;
            else if (player == Players.MIN)
                utility = 1;
        }
	else if(number == 0)
            utility = 0;
	else
	{
            Tree tempNode = childs.get(0);
            utility = tempNode.propagateUtility();
            
            for(int i=1; i<number; i++)
            {
		tempNode = childs.get(i);
		float temp = tempNode.propagateUtility();

                if(player == Players.MAX)
                    if(temp > utility)
                        utility = temp;

		if(player == Players.MIN)
                    if(temp < utility)
			utility = temp;
            }
    	}
        
        return utility;
    }

    public Tree getBestChild()
    {
        Tree bestNode = childs.get(0);
	float bestUtility = bestNode.getUtility();

        for(int i=1; i<childs.size(); i++) 
        {
            Tree nodeTemp = childs.get(i);
            float untilityTemp = nodeTemp.getUtility();

            if(player == Players.MAX)
            {
                if(untilityTemp >= bestUtility)
                {
                    bestUtility = untilityTemp;
                    bestNode = nodeTemp;
                }
            }
            else if(player == Players.MIN)
            {
                if(untilityTemp <= bestUtility)
                {
                    bestUtility = untilityTemp;
                    bestNode = nodeTemp;
                }
            }
        }
        return bestNode;
    }

    public float getUtility()
    {
	return utility;
    }
}
